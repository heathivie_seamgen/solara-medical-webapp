﻿using System.Threading.Tasks;
using SolaraMedicalWebApp.Domain.DTO;
using SolaraMedicalWebApp.Models;

namespace SolaraMedicalWebApp.Domain.Interfaces
{
    public interface IUserNotificationService
    {
        Task<bool> PublishNotificationAsync(UserNotificationDTO notification);
        Task<bool> PublishNotificationsAsync(UserNotificationBatchDTO batch);
    }
}