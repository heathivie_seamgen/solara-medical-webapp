﻿using System;
namespace SolaraMedicalWebApp.Models
{
    public class UserNotificationDTO
    {
        public int PatientID { get; set; }
        public string Message { get; set; }
    }
}
