﻿using System.Collections.Generic;

namespace SolaraMedicalWebApp.Domain.DTO
{
    public class BroadcastNotificationDTO
    {
        public string Message { get; set; }
        public List<int> PatientIDs { get; set; }

        public BroadcastNotificationDTO()
        {
            PatientIDs = new List<int>();
        }
    }
}
