﻿using System;
using System.Collections.Generic;

namespace SolaraMedicalWebApp.Domain.DTO
{
    public class UserNotificationBatchDTO
    {
        public string Message { get; set; }
        public IEnumerable<int> PatientIDs { get; set; }
    }
}
