﻿using System;

namespace SolaraMedicalWebApp.Domain
{
    public class UserNotification
    {
        public int PatientID { get; set; }
        public string Message { get; set; }
    }
}
