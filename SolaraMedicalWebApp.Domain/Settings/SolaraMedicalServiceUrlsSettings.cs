﻿using System;
namespace SolaraMedicalWebApp.Domain.Settings
{
    public class SolaraMedicalServiceUrlsSettings
    {
        public string NotificationUrl { get; set; }
        public string BatchNotificationUrl { get; set; }
    }
}
