﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using SolaraMedicalWebApp.Domain.DTO;
using SolaraMedicalWebApp.Domain.Interfaces;
using SolaraMedicalWebApp.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SolaraMedicalWebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserNotificationController : Controller
    {
        private readonly IUserNotificationService _userNotificationService;
        private readonly ILogger _logger;

        public UserNotificationController(IUserNotificationService userNotificationService, ILoggerFactory loggerFactory)
        {
            _userNotificationService = userNotificationService;
            _logger = loggerFactory.CreateLogger<UserNotificationController>();
        }

        // GET: api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public async Task<ActionResult> Post([FromBody]UserNotificationDTO model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (await _userNotificationService.PublishNotificationAsync(model).ConfigureAwait(false))
                    {
                        return Ok();
                    }
                }
                catch (Exception exception)
                {
                    _logger.LogError(0, exception, "Uploading patient list has failed");
                }
            }
            _logger.LogError("Uploading patient list has failed with invalid request {Request}", JsonConvert.SerializeObject(model));
            return BadRequest();
        }

        [HttpPost("batch")]
        public async Task<ActionResult> PostBatch([FromBody]UserNotificationBatchDTO model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (await _userNotificationService.PublishNotificationsAsync(model).ConfigureAwait(false))
                    {
                        return Ok();
                    }
                    return StatusCode(StatusCodes.Status500InternalServerError);
                }
                catch (Exception exception)
                {
                    _logger.LogError(0, exception, "Uploading patient list has failed");
                    return StatusCode(StatusCodes.Status500InternalServerError);
                }
            }
            _logger.LogError("Uploading patient list has failed with invalid request {Request}", JsonConvert.SerializeObject(model));
            return BadRequest();
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
