import React from 'react'
import FileUpload from '../components/FileUpload'

const PatientUpload = () => (
	<div>
		<div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: 'calc(100vh-100px)', paddingTop:'20px' }}>
			<div>
				<FileUpload />
			</div>
		</div>
	</div>
)

export default PatientUpload
