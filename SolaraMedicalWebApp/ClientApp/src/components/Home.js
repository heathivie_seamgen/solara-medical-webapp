import React, { Component } from 'react'
import FileUpload from '../components/FileUpload'
export class Home extends Component {
	static displayName = Home.name

	render() {
		return (
			<div>
				<div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center',height:'100vh'}}>
					<div>
						<FileUpload />
					</div>
				</div>
			</div>
		)
	}
}
