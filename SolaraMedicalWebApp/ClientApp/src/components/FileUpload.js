﻿import React, { Component } from 'react'
import injectSheet from 'react-jss'
import Upload from '../icons/Upload'
import { postbatchNotification } from '../api/patientApi'
var XLSX = require('xlsx')

const styles = {
	dropzone: {
		height: 'calc(80vh - 80px)',
		width: '80vh',
		backgroundColor: '#fff',
		border: '2px dashed rgb(187, 186, 186)',
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center',
		flexDirection: 'column',
		fontSize: '16px',
	},
	fileInput: {
		display: 'none'
	},
	highlight: {
		height: 'calc(80vh - 80px)',
		width: '80vh',
		backgroundColor: '#fff',
		border: '2px dashed #FCB034',
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center',
		flexDirection: 'column',

		fontSize: '16px'
	}
}

class FileUpload extends Component {
	constructor(props) {
		super(props)

		this.state = { highlight: false, errorMessage: null, successMessage: null }

		this.fileInputRef = React.createRef()

		this.openFileDialog = this.openFileDialog.bind(this)
		this.onFilesAdded = this.onFilesAdded.bind(this)
		this.onDragOver = this.onDragOver.bind(this)
		this.onDragLeave = this.onDragLeave.bind(this)
		this.onDrop = this.onDrop.bind(this)
	}

	openFileDialog = () => {
		if (this.props.disabled) return
		this.fileInputRef.current.click()
	}

	onFilesAdded = (evt) => {
		var reader = new FileReader()
		var self = this
		reader.onload = function(e) {
			try {
				var t = ''
				var data = new Uint8Array(e.target.result)
				var workbook = XLSX.read(data, { type: 'array' })

				var t = XLSX.utils.sheet_to_json(workbook.Sheets['Sheet1'], { range: 1 })
				if (t) {
					var d = t.map((r) => {
						if (r) return r.ID
					})
					self.uploadPatients(d)
				}
				
			} catch (error) {
				self.setState({ errorMessage: 'This file could not be parsed' })
			}
			
		}
		reader.readAsArrayBuffer(evt[0])
	}

	uploadPatients = (patients) => {
		let batch = {
			patientIDs: patients,
			message: ''
		}
		return postbatchNotification(batch)
			.then((r) => {
				if (r) {
					switch (r) {
						case 200:
							this.setState({ successMessage: 'Upload has completed successfully' })
							break
						case 400:
							this.setState({ errorMessage: 'Invalid request, please check the data format' })
							break
						case 500:
							this.setState({ errorMessage: 'Upload has failed, please try again later' })
							break
					}
				}
			})
			.catch((r) => {
				console.log(r)
			})
	}

	onDragOver = (evt) => {
		evt.preventDefault()

		if (this.props.disabled) return

		this.setState({ highlight: true })
	}

	onDragLeave = () => {
		this.setState({ highlight: false })
	}

	onDrop = (event) => {
		event.preventDefault()
		this.setState({ errorMessage: null, successMessage: null })

		if (this.props.disabled) return

		const files = event.dataTransfer.files

		this.onFilesAdded(files)

		this.setState({ highlight: false })
	}

	render = () => {
		return (
			<div>
				<div style={{ height: '20px', textAlign: 'center' }}>
					{this.state.errorMessage && <p style={{ color: 'red' }}>{this.state.errorMessage}</p>}
					{this.state.successMessage && <p style={{ color: 'green' }}>{this.state.successMessage}</p>}
				</div>
				<p style={{ textAlign: 'center', opacity: 0.7 }}>
					Please upload the list of patients that are due for reorder.
				</p>
				<div
					className={this.state.highlight ? this.props.classes.highlight : this.props.classes.dropzone}
					onDragOver={this.onDragOver}
					onDragLeave={this.onDragLeave}
					onDrop={this.onDrop}
					onClick={this.openFileDialog}
					style={{ cursor: this.props.disabled ? 'default' : 'pointer' }}
				>
					<input
						ref={this.fileInputRef}
						className={this.props.classes.fileInput}
						type="file"
						multiple
						onChange={(e) => this.onFilesAdded(e.target.files)}
					/>

					<Upload />
					<div style={{ zIndex: 1000 }}>
						<h1 style={{ paddingTop: '50px', opacity: 0.6 }}>Drap and Drop to Upload</h1>
						<p style={{ textAlign: 'center', opacity: 0.7 }}>
							or <span style={{ color: 'blue' }}>browse</span> to choose a file
						</p>
					</div>
				</div>
			</div>
		)
	}
}

export default injectSheet(styles)(FileUpload)
