import React, { Component } from 'react'
import Logo from '../icons/Logo'

export class Layout extends Component {
	render() {
		return (
			<div>
				<div style={{ height: '65px', backgroundColor: '#f8f8f8' }}>
					<div style={{ display: 'flex', alignItems:'center'}}>
						<div style={{ marginLeft: '20vw', paddingTop: '15px' }}>
							<Logo />
						</div>
					</div>
				</div>
				{this.props.children}
			</div>
		)
	}
}
