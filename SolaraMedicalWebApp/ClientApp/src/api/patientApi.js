﻿export const postbatchNotification = (batchNotification) => {
	return fetch('/api/UserNotification/batch', {
		method: 'POST', 
		mode: 'cors', 
		credentials: 'same-origin', 
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify(batchNotification) 
	}).then((response) => {
		return response.status
	}).catch(e => {
        console.log(e)
    })
}
