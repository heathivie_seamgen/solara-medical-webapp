import React, { Component } from 'react'
import { Route } from 'react-router'
import { Layout } from './components/Layout'
import PatientUpload  from './pages/PatientUpload'

export default class App extends Component {
	static displayName = App.name

	render() {
		return (
			<Layout>
				<Route exact path="/" component={PatientUpload} />
			</Layout>
		)
	}
}
