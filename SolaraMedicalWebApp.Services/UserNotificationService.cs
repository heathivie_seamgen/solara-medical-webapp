﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SolaraMedicalWebApp.Domain.DTO;
using SolaraMedicalWebApp.Domain.Interfaces;
using SolaraMedicalWebApp.Domain.Settings;
using SolaraMedicalWebApp.Models;

namespace SolaraMedicalWebApp.Services
{
    public class UserNotificationService : IUserNotificationService
    {
        private readonly IHttpClientFactory _clientFactory;
        private readonly SolaraMedicalServiceUrlsSettings _urlsSettings;

        public UserNotificationService(IHttpClientFactory clientFactory, SolaraMedicalServiceUrlsSettings urlsSettings)
        {
            _clientFactory = clientFactory;
            _urlsSettings = urlsSettings;
        }
        public async Task<bool> PublishNotificationAsync(UserNotificationDTO notification)
        {
            if (notification == null || notification.PatientID < 1)
            {
                throw new ArgumentException(nameof(notification));
            }

            var dto = new BroadcastNotificationDTO();
            dto.Message = notification.Message;
            dto.PatientIDs.Add(notification.PatientID);

            var client = _clientFactory.CreateClient();

            var response = await client.PostAsync(_urlsSettings.BatchNotificationUrl,
                new StringContent(JsonConvert.SerializeObject(notification), Encoding.UTF8, "application/json")).ConfigureAwait(false);
            return response.IsSuccessStatusCode;

        }

        public async Task<bool> PublishNotificationsAsync(UserNotificationBatchDTO batch)
        {
            if (batch == null || batch.PatientIDs == null || !batch.PatientIDs.Any())
            {
                throw new ArgumentException(nameof(batch));
            }

            var dto = new BroadcastNotificationDTO();
            dto.Message = "It's time to re-order supplies from Solara!";
            dto.PatientIDs.AddRange(batch.PatientIDs);

            var client = _clientFactory.CreateClient();

            var response = await client.PostAsync(_urlsSettings.BatchNotificationUrl,
            new StringContent(JsonConvert.SerializeObject(batch), Encoding.UTF8, "application/json")).ConfigureAwait(false);

            return response.IsSuccessStatusCode;

        }
    }
}
